signature ORD =
sig

type t

val le : t -> t -> bool

end

structure IntOrd : ORD = 
struct 
	type t = int
	fun le (a:t) (b:t) = if a>= b then true else false
end

structure RealOrd : ORD = 
struct 
	type t = real
	fun le (x:t) (y:t) = (x>= y)
end

functor Qsort (X:ORD) = 
struct 
	fun qs [] = []
	|   qs (x::xs) = let val (l,g) =List.partition (X.le x) xs
			in qs l @ [x] @ qs g
			end	
	
end
structure IntSort = Qsort(IntOrd);
val list = [2,6,8,1,9];
IntSort.qs list;

